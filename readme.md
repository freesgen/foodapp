# Foodapp prototype for cashDepot

## backend setup
```
    composer install
    php artisan migrate
    php artisan DB:seed
```

## fronted setup
```
    cd frontend/
    npm install
    npm run build
```
