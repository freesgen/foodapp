<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/currentuser', function (Request $request) {
    if (Auth::check()) {
        return ["data" => Auth::user()];
    }
    return ["error" => "no authenticated"];
});

Route::get('/logout', function () {
    Auth::logout();
    return redirect('/');
});

Route::get('/{any}', 'SpaController@index')->where('any', '.*');
