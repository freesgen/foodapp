import "./bootstrap";
import "./sass/app.scss";
import Vue from "vue";
import VeeValidate from "vee-validate";
import axios from "axios";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./plugins/element.js";

axios.defaults.baseURL = "/";
Vue.prototype.$http = axios;

Vue.use(VeeValidate);

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");
