import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: null,
        employee: null
    },
    
    mutations: {
        SET_EMPLOYEE(state, employee) {
            state.employee = employee;
        },

        SET_USER(state, user) {
            state.user = user;
        }
    },

    actions: {
        setEmployee({commit}, employee) {
            commit('SET_EMPLOYEE', employee);
        },

        setEmployee({commit}, user) {
            commit('SET_USER', user);
        }
    }
});
