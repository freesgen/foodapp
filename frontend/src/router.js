import Vue from "vue";
import axios from "axios";
import Router from "vue-router";
import Home from "./views/Home.vue";
import OrderSuccess from "./views/OrderSuccess.vue";
import Login from "./views/Login.vue";

Vue.use(Router);

const router = new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [
        {
            path: "/",
            name: "root",
            component: Home,
            meta: { requiresAuth: true }
        },
        {
            path: "/home",
            name: "home",
            component: Home,
            meta: { requiresAuth: true }
        },
        {
            path: "/order-success",
            name: "orderSuccess",
            component: OrderSuccess,
            meta: { requiresAuth: true }
        },
        {
            path: "/signin",
            name: "signin",
            component: Login
        }
    ]
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        axios
            .get("/currentuser")
            .then(({ data }) => {
                if (!data.data) {
                    next({
                        path: "/signin"
                    });
                } else {
                    next();
                }
            })
            .catch(() => {
                next({
                    path: "/signin"
                });
            });
    } else {
        next();
    }
});

export default router;
