<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/chunk-vendors.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel=icon href=/favicon.ico> <title>frontend</title>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <link href=/js/about.js rel=prefetch>
    <link href=/js/chunk-vendors.js rel=preload as=script>

    <style>
        * {
            margin: auto;
            padding: 0
        }
    </style>
</head>
<body>
    <div id="app-server">
        <main class="">
            @yield('content')
        </main>
    </div>
    <script> const API_URL = "{!! getenv('API_URL') !!}" </script>
    <script src="/js/chunk-vendors.js"> </script>
     <!-- Scripts -->
     <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>